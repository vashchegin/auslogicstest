﻿using Auslogics.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Auslogics
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Run data collection in separate thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            List<Core.Data.StartupItem> result = new List<Core.Data.StartupItem>();
            result.AddRange(StartupLoader.GetAllFromWMI());            
            result.AddRange(StartupLoader.GetAllFromScheduler());
            e.Result = result;
        }

        /// <summary>
        /// Main page loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Main_Load(object sender, EventArgs e)
        {
            backgroundWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Background worker completed event processing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ImageList imagelist = new ImageList();
            lvStartupFiles.SmallImageList = imagelist;
            List<Core.Data.StartupItem> AllStartupItems = e.Result as List<Core.Data.StartupItem>;

            if (AllStartupItems != null && AllStartupItems.Count > 0)
            {
                lvStartupFiles.Items.Clear();
                lvStartupFiles.View = View.Details;

                foreach (var item in AllStartupItems)
                {
                    imagelist.Images.Add(item.Icon);

                    ListViewItem listitem = new ListViewItem(string.Empty, imagelist.Images.Count - 1);
                    listitem.SubItems.Add(item.Name);
                    listitem.SubItems.Add(item.Parameters);
                    listitem.SubItems.Add(item.FullPath);
                    listitem.SubItems.Add(item.Type.ToString());
                    listitem.SubItems.Add(item.IsSigned.ToString());
                    listitem.SubItems.Add(item.IsCorrectSign.ToString());
                    listitem.SubItems.Add(item.Company);

                    lvStartupFiles.Items.Add(listitem);
                }

                lvStartupFiles.Columns[1].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
                lvStartupFiles.Columns[3].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
            }

            pbLoading.Visible = false;
        }

        /// <summary>
        /// Double click on row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lvStartupFiles_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var selectedRow = lvStartupFiles.SelectedItems[0];
            var fullPath = selectedRow.SubItems[3].Text;

            string argument = string.Format("/select, \"{0}\"", fullPath);
            System.Diagnostics.Process.Start("explorer.exe", argument);
        }
    }
}
