﻿namespace Auslogics
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.clmnIcon = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmnFileName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmnParams = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmnFullPath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmnLoadType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmnIsSigned = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmnIsSignCorrect = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmnCompany = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.lvStartupFiles = new System.Windows.Forms.ListView();
            this.fileIcon = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.fileName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.fileArgs = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.fileFullPath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.startupType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.isSigned = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.isCorrectSign = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.company = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.pbLoading = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clmnIcon,
            this.clmnFileName,
            this.clmnParams,
            this.clmnFullPath,
            this.clmnLoadType,
            this.clmnIsSigned,
            this.clmnIsSignCorrect,
            this.clmnCompany});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(914, 471);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.List;
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // lvStartupFiles
            // 
            this.lvStartupFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvStartupFiles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.fileIcon,
            this.fileName,
            this.fileArgs,
            this.fileFullPath,
            this.startupType,
            this.isSigned,
            this.isCorrectSign,
            this.company});
            this.lvStartupFiles.FullRowSelect = true;
            this.lvStartupFiles.Location = new System.Drawing.Point(12, 12);
            this.lvStartupFiles.MultiSelect = false;
            this.lvStartupFiles.Name = "lvStartupFiles";
            this.lvStartupFiles.Size = new System.Drawing.Size(890, 431);
            this.lvStartupFiles.TabIndex = 1;
            this.lvStartupFiles.UseCompatibleStateImageBehavior = false;
            this.lvStartupFiles.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvStartupFiles_MouseDoubleClick);
            // 
            // fileIcon
            // 
            this.fileIcon.Text = "";
            // 
            // fileName
            // 
            this.fileName.Text = "File Name";
            // 
            // fileArgs
            // 
            this.fileArgs.Text = "Arguments";
            this.fileArgs.Width = 150;
            // 
            // fileFullPath
            // 
            this.fileFullPath.Text = "Full Path";
            // 
            // startupType
            // 
            this.startupType.Text = "Type";
            // 
            // isSigned
            // 
            this.isSigned.Text = "Is Signed";
            this.isSigned.Width = 80;
            // 
            // isCorrectSign
            // 
            this.isCorrectSign.Text = "Is Correct Sign";
            this.isCorrectSign.Width = 80;
            // 
            // company
            // 
            this.company.Text = "Company";
            this.company.Width = 120;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pbLoading});
            this.toolStrip1.Location = new System.Drawing.Point(0, 446);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(914, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // pbLoading
            // 
            this.pbLoading.Name = "pbLoading";
            this.pbLoading.Size = new System.Drawing.Size(250, 22);
            this.pbLoading.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 471);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.lvStartupFiles);
            this.Controls.Add(this.listView1);
            this.Name = "Main";
            this.Text = "AuslogicsTest";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Main_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader clmnIcon;
        private System.Windows.Forms.ColumnHeader clmnFileName;
        private System.Windows.Forms.ColumnHeader clmnParams;
        private System.Windows.Forms.ColumnHeader clmnFullPath;
        private System.Windows.Forms.ColumnHeader clmnLoadType;
        private System.Windows.Forms.ColumnHeader clmnIsSigned;
        private System.Windows.Forms.ColumnHeader clmnIsSignCorrect;
        private System.Windows.Forms.ColumnHeader clmnCompany;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.ListView lvStartupFiles;
        private System.Windows.Forms.ColumnHeader fileIcon;
        private System.Windows.Forms.ColumnHeader fileName;
        private System.Windows.Forms.ColumnHeader fileArgs;
        private System.Windows.Forms.ColumnHeader fileFullPath;
        private System.Windows.Forms.ColumnHeader startupType;
        private System.Windows.Forms.ColumnHeader isSigned;
        private System.Windows.Forms.ColumnHeader isCorrectSign;
        private System.Windows.Forms.ColumnHeader company;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripProgressBar pbLoading;
    }
}

