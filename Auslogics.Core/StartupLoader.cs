﻿using Auslogics.Core.Data;
using Auslogics.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using TaskScheduler;

namespace Auslogics.Core
{
    public static class StartupLoader
    {
        private static Logger logger;

        static StartupLoader()
        {
            logger = new Logger();
        }

        /// <summary>
        /// Public method. Get all tasks from WMI
        /// </summary>
        /// <returns></returns>
        public static List<StartupItem> GetAllFromWMI()
        {
            List<StartupItem> result = new List<StartupItem>();
            try
            {
                ManagementClass cls = new ManagementClass("Win32_StartupCommand");
                ManagementObjectCollection coll = cls.GetInstances();
                
                foreach (ManagementObject obj in coll)
                {
                    string Command = obj["Command"].ToString();

                    StartupItem item = new StartupItem();
                    //item.Name = obj["Name"].ToString();
                    item.Type = GetType(obj["Location"].ToString());
                    ParseCommandAndFillData(Command, item);

                    result.Add(item);
                    
                }                
            }
            catch (Exception ex)
            {
                logger.LogError(ex);
            }

            return result;
        }

        /// <summary>
        /// Public method. Get All tasks from Scheduler
        /// </summary>
        /// <returns></returns>
        public static List<StartupItem> GetAllFromScheduler()
        {
            List<StartupItem> result = new List<StartupItem>();

            try
            {
                TaskScheduler.TaskScheduler scheduler = new TaskScheduler.TaskScheduler();
                scheduler.Connect();
                TaskScheduler.ITaskFolder root = scheduler.GetFolder("");

                ProcessScheduleFolder(root, result);
            }
            catch (Exception ex)
            {
                logger.LogError(ex);
            }

            return result;
        }

        private static void ProcessScheduleFolder(ITaskFolder root, List<StartupItem> result)
        {
            var tasks = root.GetTasks(1);
            foreach (TaskScheduler.IRegisteredTask task in tasks)
            {
                if (task.Enabled)
                {
                    bool startUp = false;
                    foreach (TaskScheduler.ITrigger trigger in task.Definition.Triggers)
                    {
                        if (trigger.Type == TaskScheduler._TASK_TRIGGER_TYPE2.TASK_TRIGGER_BOOT ||
                            trigger.Type == TaskScheduler._TASK_TRIGGER_TYPE2.TASK_TRIGGER_LOGON)
                        {
                            startUp = true;
                            break;
                        }

                    }
                    if (startUp)
                    {
                        foreach (TaskScheduler.IAction action in task.Definition.Actions)
                        {
                            if (action.Type == _TASK_ACTION_TYPE.TASK_ACTION_EXEC)
                            {
                                TaskScheduler.IExecAction execAction = action as TaskScheduler.IExecAction;
                                StartupItem item = new StartupItem();
                                item.Type = LoadType.Scheduler;

                                ParseCommandAndFillData(execAction.Path, item);
                                item.Parameters = execAction.Arguments;

                                result.Add(item);
                            }
                        }

                    }
                }

            }

            var folders = root.GetFolders(1);
            foreach (TaskScheduler.ITaskFolder folder in folders)
            {
                ProcessScheduleFolder(folder, result);
            }
        }

        /// <summary>
        /// Parse Command and Fill all needed data
        /// </summary>
        /// <param name="Command"></param>
        /// <param name="item"></param>
        private static void ParseCommandAndFillData(string Command, StartupItem item)
        {
            string fullPath, arguments;
            ExtractFullPathAndArguments(Command, out fullPath, out arguments);
            item.FullPath = fullPath;
            item.Parameters = arguments;
            item.Name = Path.GetFileName(fullPath);
            item.Exist = File.Exists(fullPath);

            GetProgramIcon(item);

            var sign = SignHelper.GetSignatureInfo(fullPath);
            item.IsSigned = sign.IsSigned;
            item.IsCorrectSign = sign.IsTrusted;
            item.Company = sign.Issuer;
            if (string.IsNullOrEmpty(item.Company))
            {
                GetCompanyFromFileVersion(item, fullPath);
            }
        }

        /// <summary>
        /// Get company name from resources
        /// </summary>
        /// <param name="item"></param>
        /// <param name="fullPath"></param>
        private static void GetCompanyFromFileVersion(StartupItem item, string fullPath)
        {
            try
            {
                var fileVersion = FileVersionInfo.GetVersionInfo(fullPath);
                item.Company = fileVersion.CompanyName;
            }
            catch (FileNotFoundException) { }
            catch (Exception ex)
            {
                logger.LogError(ex);
            }
        }

        /// <summary>
        /// Get execute file icon
        /// </summary>
        /// <param name="item"></param>
        private static void GetProgramIcon(StartupItem item)
        {
            try
            {
                item.Icon = Icon.ExtractAssociatedIcon(item.FullPath);
            }
            catch (FileNotFoundException)
            {
                item.Icon = SystemIcons.Application;
            }
            catch (Exception ex)
            {
                logger.LogError(ex);
            }
        }

        /// <summary>
        /// Extract full path to the file and arguments of command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="fullpath"></param>
        /// <param name="arguments"></param>
        private static void ExtractFullPathAndArguments(string command, out string fullpath, out string arguments)
        {
            var col = PathHelper.CommandLineToArgs(command);

            StringBuilder arg = new StringBuilder();
            fullpath = arguments = string.Empty;
            StringBuilder path = new StringBuilder();

            int idx = 0;
            while (idx < col.Length)
            {                
                path.Append(col[idx].Trim(new char[] { '\"', ' ' }));
                if (col[idx].IndexOf(".exe", StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    break;
                }
                path.Append(" ");
                idx++;                
            }
            fullpath = path.ToString();
            if (fullpath.Contains('%')) fullpath = Environment.ExpandEnvironmentVariables(fullpath);
            if (fullpath.Contains('~')) fullpath = PathHelper.GetLongPath(fullpath);
            idx++;
            while (idx < col.Length)
            {
                arg.Append(" ");
                arg.Append(col[idx].Trim(new char[] { '\"', ' ' }));
                idx++;
            }
            arguments = arg.ToString().Trim();

        }

        /// <summary>
        /// To determine is it Start Menu or Registry, both are going from WMI
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        private static LoadType GetType(string location)
        {
            if (location == "Startup" || location == "Common Startup")
            {
                return LoadType.StartMenu;
            }
            else
            {
                return LoadType.Registry;
            }
        }
    }
}
