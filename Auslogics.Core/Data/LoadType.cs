﻿namespace Auslogics.Core.Data
{
    public enum LoadType
    {
        Registry,
        StartMenu,
        Scheduler
    }
}
