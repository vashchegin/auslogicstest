﻿using System.Drawing;

namespace Auslogics.Core.Data
{
    public class StartupItem
    {
        public StartupItem() { }

        public Icon Icon { get; set; }
        public string Name { get; set; }
        public string FullPath { get; set; }
        public LoadType Type { get; set; }
        public string Parameters { get; set; }
        public bool IsSigned { get; set; }
        public bool IsCorrectSign { get; set; }
        public string Company { get; set; }
        public bool Exist { get; set; }

    }
}
