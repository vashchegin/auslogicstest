﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auslogics.Core.Data
{
    public class SignatureInfo
    {
        public bool IsSigned { get; set; }
        public bool IsTrusted { get; set; }
        public string Issuer { get; set; }
    }
}
