﻿namespace Auslogics.Core.Helpers.NestedClasses
{

    enum AllocMethod
    {
        HGlobal,
        CoTaskMem
    };

    enum UnionChoice
    {
        File = 1,
        Catalog,
        Blob,
        Signer,
        Cert
    };

    enum UiChoice
    {
        All = 1,
        NoUI,
        NoBad,
        NoGood
    };

    enum RevocationCheckFlags
    {
        None = 0,
        WholeChain
    };

    enum StateAction
    {
        Ignore = 0,
        Verify,
        Close,
        AutoCache,
        AutoCacheFlush
    };

    enum TrustProviderFlags
    {
        UseIE4Trust = 1,
        NoIE4Chain = 2,
        NoPolicyUsage = 4,
        RevocationCheckNone = 16,
        RevocationCheckEndCert = 32,
        RevocationCheckChain = 64,
        RecovationCheckChainExcludeRoot = 128,
        Safer = 256,
        HashOnly = 512,
        UseDefaultOSVerCheck = 1024,
        LifetimeSigning = 2048
    };

    enum UIContext
    {
        Execute = 0,
        Install
    };
}
