﻿using Auslogics.Core.Data;
using Auslogics.Core.Helpers.NestedClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Auslogics.Core.Helpers
{
    public static class SignHelper
    {
        [DllImport("Wintrust.dll", PreserveSig = true, SetLastError = false)]
        private static extern uint WinVerifyTrust(IntPtr hWnd, IntPtr pgActionID, IntPtr pWinTrustData);

        /// <summary>
        /// Verify is signature trusted or not
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static uint WinVerifyTrust(string fileName)
        {

            Guid wintrust_action_generic_verify_v2 = new Guid("{00AAC56B-CD44-11d0-8CC2-00C04FC295EE}");
            uint result = 0;
            using (WINTRUST_FILE_INFO fileInfo = new WINTRUST_FILE_INFO(fileName, Guid.Empty))
            using (UnmanagedPointer guidPtr = new UnmanagedPointer(Marshal.AllocHGlobal(Marshal.SizeOf(typeof(Guid))), AllocMethod.HGlobal))
            using (UnmanagedPointer wvtDataPtr = new UnmanagedPointer(Marshal.AllocHGlobal(Marshal.SizeOf(typeof(WINTRUST_DATA))), AllocMethod.HGlobal))
            {
                WINTRUST_DATA data = new WINTRUST_DATA(fileInfo);
                IntPtr pGuid = guidPtr;
                IntPtr pData = wvtDataPtr;
                Marshal.StructureToPtr(wintrust_action_generic_verify_v2, pGuid, true);
                Marshal.StructureToPtr(data, pData, true);
                result = WinVerifyTrust(IntPtr.Zero, pGuid,pData);

            }
            return result;

        }

        /// <summary>
        /// Get signature info
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static SignatureInfo GetSignatureInfo(string filename)
        {
            SignatureInfo result = new SignatureInfo();
            try
            {
                X509Certificate certfile = X509Certificate.CreateFromSignedFile(filename);
                X509Certificate2 cert = new X509Certificate2(certfile);

                result.Issuer = cert.GetNameInfo(X509NameType.SimpleName, false);                
                
                result.IsSigned = true;
                result.IsTrusted = IsTrusted(filename);
            }
            catch
            {
                result.IsSigned = false;
                result.IsTrusted = false;
                result.Issuer = string.Empty;
            }
            return result;
        }

        /// <summary>
        /// Public method to check signature trust
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool IsTrusted(string fileName)
        {
            return WinVerifyTrust(fileName) == 0;
        }
    }

}
