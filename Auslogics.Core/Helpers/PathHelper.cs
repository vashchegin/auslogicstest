﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace Auslogics.Core.Helpers
{
    public static class PathHelper
    {
        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        private static extern int GetLongPathName(
                   [MarshalAs(UnmanagedType.LPTStr)]
                   string path,
                   [MarshalAs(UnmanagedType.LPTStr)]
                   StringBuilder longPath,
                   int longPathLength
                   );

        [DllImport("shell32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr CommandLineToArgvW(
        [MarshalAs(UnmanagedType.LPWStr)] string lpCmdLine, out int pNumArgs);

        /// <summary>
        /// Get long path from short
        /// </summary>
        /// <param name="shortPath"></param>
        /// <returns></returns>
        public static string GetLongPath(string shortPath)
        {
            StringBuilder longPath = new StringBuilder(255);
            GetLongPathName(shortPath, longPath, longPath.Capacity);
            return longPath.ToString();
        }

        

        /// <summary>
        /// Parse a string into an array, including items in quotes
        /// </summary>
        /// <param name="commandLine"></param>
        /// <returns></returns>
        public static string[] CommandLineToArgs(string commandLine)
        {
            if (String.IsNullOrEmpty(commandLine))
                return new string[] { };

            int argc;
            var argv = CommandLineToArgvW(commandLine, out argc);
            if (argv == IntPtr.Zero)
                throw new System.ComponentModel.Win32Exception();
            try
            {
                var args = new string[argc];
                for (var i = 0; i < args.Length; i++)
                {
                    var p = Marshal.ReadIntPtr(argv, i * IntPtr.Size);
                    args[i] = Marshal.PtrToStringUni(p);
                }

                return args;
            }
            finally
            {
                Marshal.FreeHGlobal(argv);
            }
        }
    }
}
