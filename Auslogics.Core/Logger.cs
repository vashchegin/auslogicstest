﻿using System;
using System.IO;

namespace Auslogics.Core
{
    public class Logger
    {
        private static StreamWriter logger;

        public Logger(string fileName = "log.txt")
        {
            logger = new StreamWriter(fileName);
            logger.AutoFlush = true;
        }

        public void LogError(Exception ex)
        {
            LogError(ex.ToString());
        }

        public void LogError(string message)
        {
            Log(message, "ERROR");
        }

        public void LogWarning(string message)
        {
            Log(message, "WARNING");
        }

        public void LogInfo(string message)
        {
            Log(message, "INFO");
        }

        private void Log(string message, string severity)
        {
            logger.WriteLine(string.Format("{0} {1} {3}: {2}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString(), message, severity));
        }

    }
}
